#include <iostream>
#include <chrono>

class Marble {
private:
  Marble* left;
  Marble* right;
  unsigned int number;

public:
  Marble() {
    this->number = 0;
  }

  Marble(unsigned int number) {
    this->left = this;
    this->right = this;
    this->number = number;
  }

  Marble(unsigned int number, Marble* left, Marble* right) {
    this->left = left;
    this->right = right;
    this->number = number;
  }

  ~Marble() {
    this->left->right = this->right;
    this->right->left = this->left;
  }

  void reset(unsigned int number) {
    this->left = this;
    this->right = this;
    this->number = number;
  }

  Marble* insert_right(Marble* new_marble) {
    new_marble->left = this;
    new_marble->right = this->right;

    this->right->left = new_marble;
    this->right = new_marble;

    return new_marble;
  }

  inline Marble* get_left() const {
    return left;
  }

  inline Marble* get_left6() const {
    return left->left->left->left->left->left;
  }

  inline Marble* get_right() const {
    return right;
  }

  void set_number(unsigned int number) {
    this->number = number;
  }

  unsigned int get_number() const {
    return number;
  }
};

unsigned int run(const unsigned int num_players, const unsigned int last_marble) {
  Marble* buffer = new Marble[last_marble];
  size_t buffer_next = 1;
  unsigned int* scores = new unsigned int[num_players];
  unsigned int current_turn = 0;

  Marble* current = buffer;
  current->reset(0);

  for (unsigned int next_marble = 1; next_marble <= last_marble; ++next_marble) {
    if (next_marble % 23 == 0) {
      current = current->get_left6();
      Marble* seven_left = current->get_left();

      scores[current_turn] += next_marble + seven_left->get_number();
    } else {
      current = current->get_right()->insert_right(buffer + buffer_next++);
      current->set_number(next_marble);
    }

    current_turn = (current_turn + 1) % num_players;
  }

  unsigned int max_score = 0;
  for (unsigned int i = 0; i < num_players; ++i) {
    if (scores[i] > max_score) {
      max_score = scores[i];
    }
  }

  return max_score;
}

int main() {
	auto start = std::chrono::high_resolution_clock::now();
  auto p1 = run(452, 70784);
  auto part2 = std::chrono::high_resolution_clock::now();
  auto p2 = run(452, 7078400);
  auto elapsed2 = std::chrono::high_resolution_clock::now() - part2;
  auto elapsed1 = part2 - start;
	long long microseconds1 = std::chrono::duration_cast<std::chrono::microseconds>(elapsed1).count();
	long long microseconds2 = std::chrono::duration_cast<std::chrono::microseconds>(elapsed2).count();
  std::cout << "Result  1: " << p1 << std::endl;
  std::cout << "Runtime 1: " << microseconds1 << " µS\n";
	std::cout << "Result  2: " << p2 << std::endl;
	std::cout << "Runtime 2: " << microseconds2 << " µS\n";
}