pub struct Interator<'a> {
    iter: std::str::Chars<'a>,
}

const ASCII_ZERO: u8 = '0' as u8;

impl<'a> Iterator for Interator<'a> {
    type Item = i32;

    fn next(&mut self) -> Option<i32> {
        let mut result = 0;
        let mut negative = false;
        let mut found = false;

        loop {
            if let Some(ch) = self.iter.next() {    
                match ch {
                    '-' => negative = true,
                    '0'...'9' => {
                        found = true;
                        result = (result * 10) + ((ch as u8) - ASCII_ZERO) as i32
                    },
                    _ => {
                        if found {
                            return if negative { Some(-result) } else { Some(result) }
                        }
                    },
                } 
            } else {
                if found {
                    return if negative { Some(-result) } else { Some(result) }
                }

                break;
            }
        }

        None
    }
}

impl<'a> Interator<'a> {
    pub fn from_iter(iter: std::str::Chars<'a>) -> Interator<'a> {
        Interator{iter}
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_one_digit() {
        let mut iter = Interator::from_iter("There are 5ome numb3r5 in this str-1ng.".chars());

        assert_eq!(iter.next().unwrap(), 5);
        assert_eq!(iter.next().unwrap(), 3);
        assert_eq!(iter.next().unwrap(), 5);
        assert_eq!(iter.next().unwrap(), -1);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_multi_digit() {
        let mut iter = Interator::from_iter("[43, 29, 38, 58, -233, 1, 5674]".chars());

        assert_eq!(iter.next().unwrap(), 43);
        assert_eq!(iter.next().unwrap(), 29);
        assert_eq!(iter.next().unwrap(), 38);
        assert_eq!(iter.next().unwrap(), 58);
        assert_eq!(iter.next().unwrap(), -233);
        assert_eq!(iter.next().unwrap(), 1);
        assert_eq!(iter.next().unwrap(), 5674);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_single_delimiter() {
        let mut iter = Interator::from_iter("32,-23,1100,2".chars());

        assert_eq!(iter.next().unwrap(), 32);
        assert_eq!(iter.next().unwrap(), -23);
        assert_eq!(iter.next().unwrap(), 1100);
        assert_eq!(iter.next().unwrap(), 2);
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_no_numbers() {
        let mut iter = Interator::from_iter("A sad line without any numbers.".chars());

        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_day10_line() {
        let mut iter = Interator::from_iter("position=<-31604,  10726> velocity=< 3, -1>".chars());

        assert_eq!(iter.next().unwrap(), -31604);
        assert_eq!(iter.next().unwrap(), 10726);
        assert_eq!(iter.next().unwrap(), 3);
        assert_eq!(iter.next().unwrap(), -1);
        assert_eq!(iter.next(), None);
    }
}