/*
use std::cmp::{PartialEq, max, min};
use std::marker::Copy;
use std::clone::Clone;
use std::fmt::Debug;

#[derive(PartialEq, Copy, Clone, Debug)]
*/

pub struct Rectangle {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}

impl Rectangle {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Rectangle {
        return Rectangle{x, y, w, h};
    }

    pub fn parse_separate(xy: &str, wh: &str) -> Option<Rectangle> {
        let xy_split: Vec<&str> = xy.split(|c| c == 'x' || c == ',').collect();
        if xy_split.len() < 2 {
            return None;
        }
        let wh_split: Vec<&str> = wh.split(|c| c == 'x' || c == ',').collect();
        if wh_split.len() < 2 {
            return None;
        }

        let x: i32 = xy_split[0].parse().unwrap_or(0);
        let y: i32 = xy_split[1].parse().unwrap_or(0);
        let w: i32 = wh_split[0].parse().unwrap_or(0);
        let h: i32 = wh_split[1].parse().unwrap_or(0);

        Some(Rectangle::new(x, y, w, h))
    }
}