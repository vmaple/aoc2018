use std::collections::HashMap;
use std::sync::Mutex;
use std::process;
use time::PreciseTime;

lazy_static! {
    static ref HASHMAP: Mutex<HashMap<u32, fn(&str) -> ()>> = {
        Mutex::new(HashMap::new())
    };    
}

pub fn register_day_handler(day: u32, part: u32, callback: fn(&str) -> ()) {
	let mut map = HASHMAP.lock().unwrap();
	let key = (day * 10) + part;

	map.insert(key, callback);
}

pub fn run(day: u32, part: u32, input: &str) {
	// Find handler
	let key = (day * 10) + part;
	let map = HASHMAP.lock().unwrap();
	let callback = map.get(&key);

	// Run handler
	if let Some(callback) = callback {
		let start = PreciseTime::now();
		callback(input);
		let end = PreciseTime::now();

		println!("Duration: {}µs", start.to(end).num_microseconds().unwrap());
	} else {
		eprintln!("No handler registerd for day {} part {}", day, part);
		process::exit(1);
	}	
}