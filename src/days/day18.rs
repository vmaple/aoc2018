struct Collection {
  width: usize,
  height: usize,
  data: Vec<char>,
  next: Vec<char>,

  lumberyards: usize,
  woodlands: usize,
}

impl Collection {
  
  #[allow(dead_code)]
  pub fn print(&self) {
    let iter = &mut self.data.iter();

    for _ in 0..self.height {
      println!("{}", iter.take(self.width).collect::<String>());
    }

    println!("");
  }

  pub fn get(&self, x: usize, y: usize) -> char {
    if x >= self.width || y >= self.height {
      '.'
    } else {
      self.data[(y * self.width) + x]
    }
  }

  pub fn set(&mut self, x: usize, y: usize, ch: char) {
    let prev = self.next[(y * self.width) + x];
    self.next[(y * self.width) + x] = ch;

    if ch != prev {
      if prev == '|' {
        self.woodlands -= 1;
      } else if prev == '#' {
        self.lumberyards -= 1;
      }
      if ch == '|' {
        self.woodlands += 1;
      } else if ch == '#' {
        self.lumberyards += 1;
      }
    } 
  }

  pub fn commit(&mut self) {
    self.data.clear();
    self.data.extend(self.next.iter());
  }

  pub fn both_adjacent(&self, x: usize, y: usize, ch1: char, ch2: char) -> bool {
    let mut has1 = false;
    let mut has2 = false;

    for xo in 0..=2 {
      for yo in 0..=2 {
        if xo == 1 && yo == 1 {
          continue;
        }

        let ch = self.get(x - 1 + xo, y - 1 + yo);
        if ch == ch1 {
          has1 = true;

          if has2 {
            return true
          }
        }
        if ch == ch2 {
          has2 = true;

          if has1 {
            return true
          }
        }
      }
    }

    false
  }
  
  pub fn three_adjacent(&self, x: usize, y: usize, ch: char) -> bool {
    let mut count = 0;
    
    for xo in 0..=2 {
      for yo in 0..=2 {
        if xo == 1 && yo == 1 {
          continue;
        }

        if self.get(x - 1 + xo, y - 1 + yo) == ch {
          count += 1;

          if count >= 3 {
            return true
          }
        }
      }
    }

    false
  }

  pub fn resource_value(&self) -> usize {
    self.woodlands * self.lumberyards
  }

  pub fn parse(input: &str) -> Collection {
    let mut width = 0;
    let mut height = 0;
    let mut data: Vec<char> = Vec::with_capacity(1024);
    let mut lumberyards = 0;
    let mut woodlands = 0;

    for line in input.lines().filter(|l| l.len() > 0) {
      width = line.len();
      height += 1;
      
      data.extend(line.chars());
    }

    for ch in data.iter() {
      match ch {
        '|' => woodlands += 1,
        '#' => lumberyards += 1,
        _ => {},
      }
    }

    Collection{
      next: data.clone(),
      woodlands, lumberyards,
      width, height, data, 
    }
  }
}

struct Simulation {
  coll: Collection,
}

impl Simulation {
  #[allow(dead_code)]
  pub fn print(&self) {
    self.coll.print();
  }

  pub fn resource_value(&self) -> usize {
    self.coll.resource_value()
  }

  pub fn tick(&mut self) {
    for x in 0..self.coll.width {
      for y in 0..self.coll.height {
        match self.coll.get(x, y) {
          '.' => {
            if self.coll.three_adjacent(x, y, '|') {
              self.coll.set(x, y, '|');
            }
          },
          '|' => {
            if self.coll.three_adjacent(x, y, '#') {
              self.coll.set(x, y, '#');
            }
          },
          '#' => {
            if !self.coll.both_adjacent(x, y, '#', '|') {
              self.coll.set(x, y, '.');
            }
          },
          _ => panic!("Unrecongized acre: {}", self.coll.get(x, y))
        }
      }
    }

    self.coll.commit();
  }
}

pub fn part1(input: &str) {
  let mut sim = Simulation{coll: Collection::parse(input)};

  for _ in 0..10 {
    sim.tick();
  }

  println!("Result: {}", sim.resource_value());
}

pub fn part2(input: &str) {
  let mut sim = Simulation{coll: Collection::parse(input)};
  let mut last_seen = [0usize; 786432];
  let mut seen_twice = [false; 786432];
  let mut n = 0;

  while n < 1_000_000_000 {
    sim.tick();
    n += 1;

    let rv = sim.resource_value();

    let last_seen = last_seen.get_mut(rv).unwrap();
    if *last_seen > 0 {
      if seen_twice[rv] {
        let length = n - *last_seen;
        
        while n < (1_000_000_000 - length) {
          n += length;
        }
      } else {
        seen_twice[rv] = true;
        *last_seen = n;
      }
    } else {
      *last_seen = n;
    }
  }

  println!("Result: {} ({})", sim.resource_value(), n);
}