const A_CODE: usize = 'a' as usize;

pub fn part1(input: &str) {
    let mut n2_count = 0;
    let mut n3_count = 0;
    let mut letter_counts = [0u32; 26];

    for line in input.lines() {
        let mut n2_found = false;
        let mut n3_found = false;

        for i in 0..26 {
            letter_counts[i] = 0;
        }

        for ch in line.chars() {
            letter_counts[ch as usize - A_CODE] += 1;
        }

        for count in letter_counts.iter() {
            if *count == 3 {
                n3_found = true;

                if n2_found {
                    break;
                }
            } else if *count == 2 {
                n2_found = true;

                if n3_found {
                    break;
                }
            }
        }

        if n3_found {
            n3_count += 1;
        }
        if n2_found {
            n2_count += 1;
        }
    }

    println!("Result: {}", n2_count * n3_count);
}

pub fn part2(input: &str) {
    let mut winner: Vec<char> = Vec::with_capacity(128);
    let mut buffer: Vec<char> = Vec::with_capacity(128);

    for line in input.lines() {
        for line2 in input.lines() {
            if line == line2 {
                continue;
            }

            buffer.clear();
            for (a, b) in line.chars().zip(line2.chars()) {
                if a == b {
                    buffer.push(a);
                }
            }

            if buffer.len() > winner.len() {
                winner.clear();
                winner.extend(buffer.iter().cloned());
            }
        }
    }

    println!("Result: {}", winner.iter().collect::<String>());
}