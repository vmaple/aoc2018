use std::cmp::{Ord, Ordering};

static LEFT: usize = 0;
static UP: usize = 1;
static RIGHT: usize = 2;
static DOWN: usize = 3;

static DIRECTIONS: &[(isize, isize)] = &[(-1, 0), (0, -1), (1, 0), (0, 1)];

static FS_TURNS: &[usize] = &[DOWN, RIGHT, UP, LEFT];
static BS_TURNS: &[usize] = &[UP, LEFT, DOWN, RIGHT];
static INTERSECTION_TURNS: &[&[usize]] = &[&[DOWN, LEFT, UP], &[LEFT, UP, RIGHT], &[UP, RIGHT, DOWN], &[RIGHT, DOWN, LEFT]];

#[derive(Clone)]
struct Cart {
    x: usize,
    y: usize,
    direction: usize,
    next_turn: usize,
}

impl PartialEq for Cart {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y && self.direction == other.direction
    }
}
impl Eq for Cart {}
impl PartialOrd for Cart {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Cart {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.y, self.x).cmp(&(other.y, other.x))
    }
}

struct Board {
    data: Vec<char>,
    carts: Vec<Cart>,
    width: usize,
    height: usize,
}

impl Board {
    #[allow(dead_code)]
    pub fn print(&self) {
        for y in 0..self.height {
            for x in 0..self.width {
                if let Some(cart) = self.carts.iter().find(|c| c.x == x && c.y == y) {
                    match cart.direction {
                        0 => print!("<"),
                        1 => print!("^"),
                        2 => print!(">"),
                        3 => print!("v"),
                        _ => print!("Ø"),
                    }
                } else {
                    print!("{}", self.data[(y * self.width + x)]);
                }

            }

            print!("\n");
        }

        print!("\n");
    }

    pub fn last_cart_standing(&self) -> Option<&Cart> {
        if self.carts.len() == 1 {
            self.carts.get(0)
        } else {
            None
        }
    }

    pub fn tick(&mut self) -> Option<(usize, usize)> {
        let mut positions: Vec<(usize, usize)> = self.carts.iter().map(|c| (c.x, c.y)).collect();
        let mut deads = vec![false; positions.len()];
        let mut crash: Option<(usize, usize)> = None;
        
        for i in 0..self.carts.len() {
            if deads[i] {
                continue;
            }

            let cart = self.carts.get_mut(i).unwrap();
            let track = self.data[(cart.y * self.width + cart.x)];

            match track {
                '/' => {
                    cart.direction = FS_TURNS[cart.direction];
                },
                '\\' => {
                    cart.direction = BS_TURNS[cart.direction];
                },
                '+' => {
                    cart.direction = INTERSECTION_TURNS[cart.direction][cart.next_turn];
                    cart.next_turn = (cart.next_turn + 1) % 3;
                },

                '|' | '-' | ' ' => {},
                _ => {
                    println!("Unhandled {} at {},{}", track, cart.x, cart.y);
                },
            }

            let (dir_x, dir_y) = DIRECTIONS[cart.direction];

            cart.x = ((cart.x as isize) + dir_x) as usize;
            cart.y = ((cart.y as isize) + dir_y) as usize;

            if let Some((j, pos)) = positions.iter().enumerate().find(|(_, p)| **p == (cart.x, cart.y)) {
                deads[j] = true;
                deads[i] = true;
                
                crash = Some(*pos);
            }

            positions[i] = (cart.x, cart.y);
        }

        if let Some(crash) = crash {
            let mut killed = 0;
            for (i, _) in deads.iter().enumerate().filter(|(_, dead)| **dead) {
                self.carts.remove(i - killed);
                killed += 1;
            }

            return Some(crash);
        }

        self.carts.sort();

        None
    }

    pub fn from_input(input: &str) -> Board {
        let mut data: Vec<char> = Vec::with_capacity(8192);
        let mut carts: Vec<Cart> = Vec::with_capacity(32);
        let mut width: usize = 0;
        let mut height: usize = 0;

        for (y, line) in input.lines().enumerate() {
            if y == 0 {
                width = line.len();
            }
            height += 1;
            
            for (x, ch) in line.chars().enumerate() {
                match ch {
                    '<' => { 
                        carts.push(Cart{x, y, direction: LEFT, next_turn: LEFT});
                        data.push('-');
                    },
                    '>' => {
                        carts.push(Cart{x, y, direction: RIGHT, next_turn: LEFT});
                        data.push('-');
                    },
                    '^' => {
                        carts.push(Cart{x, y, direction: UP, next_turn: LEFT});
                        data.push('|');
                    },
                    'v' => {
                        carts.push(Cart{x, y, direction: DOWN, next_turn: LEFT});
                        data.push('|');
                    },
                    _ => data.push(ch),
                }
            }
        }

        Board {data, carts, width, height}
    }
}

#[allow(dead_code)]
pub fn part1(input: &str) {
    let mut board = Board::from_input(input);

    loop {
        if let Some((crash_x, crash_y)) = board.tick() {
            println!("Result: {},{}", crash_x, crash_y);
            return;
        }
    }
}

#[allow(dead_code)]
pub fn part2(input: &str) {
    let mut board = Board::from_input(input);

    loop {
        if let Some(_) = board.tick() {
            if let Some(cart) = board.last_cart_standing() {
                println!("Result: {},{}", cart.x, cart.y);
                return;
            }
        }
    }
}

#[allow(dead_code)]
pub fn combined(input: &str) {
    let mut board = Board::from_input(input);
    let mut first = true;

    loop {
        if let Some((crash_x, crash_y)) = board.tick() {
            if first {
                first = false;
                println!("P1: {},{}", crash_x, crash_y);
            }

            if let Some(cart) = board.last_cart_standing() {
                println!("P2: {},{}", cart.x, cart.y);
                return;
            }
        }
    }
}