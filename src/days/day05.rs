pub fn process_reactions<I>(input: I, buf: &mut Vec<char>) where I: Iterator<Item = char> {
    buf.clear();
    
    for ch in input {
        if ch == '\n' {
            continue;
        }

        if let Some(prev) = buf.last() {
            if reacts(ch, *prev) {
                buf.pop();
            } else {
                buf.push(ch);
            }
        } else {
            buf.push(ch);
        }
    }
}

pub fn part1(input: &str) {
    let mut buf: Vec<char> = Vec::with_capacity(input.len());
    process_reactions(input.chars(), &mut buf);

    println!("Result: {}", buf.len());
}

pub fn part2(input: &str) {
    let mut buf: Vec<char> = Vec::with_capacity(input.len());
    let mut buf2: Vec<char> = Vec::with_capacity(input.len());
    let mut winner = 0;

    process_reactions(input.chars(), &mut buf);

    for ch in "abcdefghijklmnopqrstuvwxyz".chars() {
        let ch_upper = toupper(ch);
        process_reactions(buf.iter().filter(|c| **c != ch && **c != ch_upper).map(|c| *c), &mut buf2);

        if winner == 0 || buf2.len() < winner {
            winner = buf2.len();
        }
    }

    println!("Result: {}", winner);
}

// This is ugly, but input is ascii-only.
#[inline]
fn reacts(a: char, b: char) -> bool {
    a as i32 ^ 32 == b as i32
}

#[inline]
fn toupper(ch: char) -> char {
    ((ch as u8) - 32) as char
}