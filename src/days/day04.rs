#[derive(PartialEq)]
enum Kind {
    Change,
    Wake,
    Sleep,
}

struct Event {
    kind: Kind,
    id: u32,
    minute: i32,
}

impl Event {
    fn parse(line: &str, prev_id: u32) -> Event {
        let split: Vec<&str> = line.split_whitespace().collect();

        let mut kind = Kind::Wake;
        let mut id = prev_id;

        let op = *split.get(2).unwrap();
        let id_str = *split.get(3).unwrap();

        let hhmm = split.get(1).unwrap();
        let hh: String = hhmm.chars().take(2).collect();
        let mm: String = hhmm.chars().skip(3).take(2).collect();

        let hour: u32 = hh.parse().unwrap();
        let mut minute: i32 = mm.parse().unwrap();
        if hour == 23 {
            minute -= 60;
        }

        if op == "falls" {
            kind = Kind::Sleep
        } else if op == "Guard" {
            kind = Kind::Change;
            id = id_str[1..].parse().unwrap();
        }

        Event{
            kind,
            id,
            minute,
        }
    }
}

pub fn both_parts(input: &str) {
    let mut lines: Vec<&str> = input.lines().collect();
    lines.sort();

    let mut grid = [[0u32; 60]; 4000];
    let mut totals = [0u32; 4000];
    let mut winner: usize = 0;

    let mut prev_id = 0;
    let mut prev_event: Option<Event> = None;
    for line in lines {
        let event = Event::parse(line, prev_id);

        if let Some(prev_event) = prev_event {
            if prev_event.kind == Kind::Sleep && event.kind == Kind::Wake {
                for m in prev_event.minute..event.minute {
                    if m < 0 {
                        continue;
                    }

                    grid[event.id as usize][m as usize] += 1;
                    totals[event.id as usize] += 1;

                    if totals[event.id as usize] > totals[winner] {
                        winner = event.id as usize;
                    }
                }
            }
        }

        prev_id = event.id;
        prev_event = Some(event);
    }

    let mut sleepiest_minute: usize = 0;
    for min in 0..60 {
        if grid[winner][min] > grid[winner][sleepiest_minute] {
            sleepiest_minute = min
        }
    }

    let mut best_minute_guard: usize = 0;
    let mut best_minute: usize = 0;
    for guard in 0..4000 {
        for min in 0..60 {
            if grid[guard][min] > grid[best_minute_guard][best_minute] {
                best_minute_guard = guard;
                best_minute = min;
            }
        }
    }


    println!("Sleeper (P1): {}", winner);
    println!("Total (P1): {}", totals[winner]);
    println!("Minute (P1): {} ({} days)", sleepiest_minute, grid[winner][sleepiest_minute]);
    println!("Result Part 1: {}", (winner as u32) * (sleepiest_minute as u32));
    println!("Best Minute (P2): {}", best_minute);
    println!("Best Guard (P2): {}", best_minute_guard);
    println!("Result Part 2: {}", best_minute * best_minute_guard);
}