const NO_COORD: usize = 255;

struct Coords {
    coords: Vec<(i32, i32)>,
    min_x: i32,
    min_y: i32,
    max_x: i32,
    max_y: i32,
}

fn parse_coords(input: &str) -> Coords {
    let mut coords: Vec<(i32, i32)> = Vec::with_capacity(64);
    let mut min_x = 999999;
    let mut min_y = 999999;
    let mut max_x = 0;
    let mut max_y = 0;

    for line in input.lines() {
        let split: Vec<i32> = line.split(", ").map(|t| t.parse::<i32>().unwrap()).collect();
        let x = split[0];
        let y = split[1];
        coords.push((x, y));
        if coords.len() > 254 {
            panic!("Only 254 coords supported")
        }

        if x < min_x {
            min_x = x;
        } else if x > max_x {
            max_x = x;
        }

        if y < min_y {
            min_y = y;
        } else if y > max_y {
            max_y = y;
        }
    }

    Coords{
        coords,
        min_x,
        min_y,
        max_x,
        max_y,
    }
}

fn manhattan_distance(x: i32, y: i32, x2: i32, y2: i32) -> i32 {
    (x - x2).abs() + (y - y2).abs()
}

fn get_goal(len: usize) -> i32 {
    if len > 8 {
        10000
    } else {
        32
    }
}

pub fn part1(input: &str) {
    let Coords {coords, min_x, min_y, max_x, max_y} = parse_coords(input);
    let mut grid = [[0u8; 400]; 400];
    let mut infinite = [false; 256];
    let mut area = [0u32; 256];
    let mut biggest_index = 0;

    for x in min_x..max_x+1 {
        for y in min_y..max_y+1 {
            let mut winner_distance = -1;
            let mut winner_index = 0;

            for (i, (x2, y2)) in coords.iter().enumerate() {
                let distance = manhattan_distance(x, y, *x2, *y2);

                if winner_distance == -1 || distance < winner_distance {
                    winner_distance = distance;
                    winner_index = i;

                    grid[x as usize][y as usize] = winner_index as u8;
                } else if distance == winner_distance {
                    winner_index = NO_COORD;
                }
            }

            if x == min_x || x == max_x || y == min_y || y == max_y {
                infinite[winner_index] = true;
            }

            if !infinite[winner_index] && winner_index != NO_COORD {
                area[winner_index] += 1;

                if area[winner_index] > area[biggest_index] {
                   biggest_index = winner_index;
                }
            }
        }
    }

    println!("Result: {}", area[biggest_index]);
}

pub fn part2(input: &str) {
    let Coords {coords, min_x, min_y, max_x, max_y} = parse_coords(input);
    let goal = get_goal(coords.len());
    let mut area = 0;
    let mut prev_found_any = false;

    for y in min_y..max_y {
        let mut found_any = false;
        let mut found_prev = false;

        for x in min_x..max_x {
            let sum: i32 = coords.iter().map(|(x2, y2)| manhattan_distance(x, y, *x2, *y2) as i32).sum();

            if sum < goal {
                found_any = true;
                found_prev = true;
                area += 1;
            } else if found_prev {
                // If we're past the circle, then it's safe to call it for this row.
                break;
            }
        }

        // If the entire row was empty, it's safe to assume we're done here.
        if !found_any && prev_found_any {
            break;
        }
        prev_found_any = found_any;
    }

    println!("Result: {}", area);
}