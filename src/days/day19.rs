const ADDR: usize = 0;
const ADDI: usize = 1;
const MULR: usize = 2;
const MULI: usize = 3;
const BANR: usize = 4;
const BANI: usize = 5;
const BORI: usize = 6;
const BORR: usize = 7;
const SETR: usize = 8;
const SETI: usize = 9;
const GTIR: usize = 10;
const GTRI: usize = 11;
const GTRR: usize = 12;
const EQIR: usize = 13;
const EQRI: usize = 14;
const EQRR: usize = 15;
const IP: usize = 16;
const UNKNOWN: usize = 17;

struct Op {
  code: usize,
  #[allow(dead_code)]
  name: String,
  a: usize,
  b: usize,
  c: usize,
}

impl Op {
  pub fn parse(line: &str) -> Op {
    let split = &mut line.split(' ');

    let op_name = split.next().unwrap();
    let op_code = match op_name {
      "addr" => ADDR,
      "addi" => ADDI,
      "mulr" => MULR,
      "muli" => MULI,
      "banr" => BANR,
      "bani" => BANI,
      "bori" => BORI,
      "borr" => BORR,
      "setr" => SETR,
      "seti" => SETI,
      "gtir" => GTIR,
      "gtri" => GTRI,
      "gtrr" => GTRR,
      "eqir" => EQIR,
      "eqri" => EQRI,
      "eqrr" => EQRR,
      "#ip" => IP,
      _ => UNKNOWN,
    };

    let ints = &mut split.map(|s| s.parse::<usize>().unwrap());
    let a = ints.next().unwrap();
    let b = ints.next().unwrap_or(0);
    let c = ints.next().unwrap_or(0);

    Op{
      name: String::from(op_name),
      code: op_code,
      a, b, c,
    }
  }
}

struct Program {
  registers: Vec<usize>,
  ip: usize,
  ops: Vec<Op>,
}

impl Program {
  fn exec(&mut self) -> bool {
    let ic = self.registers[self.ip];
    let op = &self.ops[ic];

    // println!("{} {}({}, {}, {}) {:?}", ic, op.name, op.a, op.b, op.c, self.registers);

    match op.code {
      ADDR => self.registers[op.c] = self.registers[op.a] + self.registers[op.b],
      ADDI => self.registers[op.c] = self.registers[op.a] + op.b,
      MULR => self.registers[op.c] = self.registers[op.a] * self.registers[op.b],
      MULI => self.registers[op.c] = self.registers[op.a] * op.b,
      BANR => self.registers[op.c] = self.registers[op.a] & self.registers[op.b],
      BANI => self.registers[op.c] = self.registers[op.a] & op.b,
      BORR => self.registers[op.c] = self.registers[op.a] | self.registers[op.b],
      BORI => self.registers[op.c] = self.registers[op.a] | op.b,
      SETR => self.registers[op.c] = self.registers[op.a],
      SETI => self.registers[op.c] = op.a,
      GTIR => self.registers[op.c] = if op.a > self.registers[op.b] { 1 } else { 0 },
      GTRI => self.registers[op.c] = if self.registers[op.a] > op.b { 1 } else { 0 },
      GTRR => self.registers[op.c] = if self.registers[op.a] > self.registers[op.b] { 1 } else { 0 },
      EQIR => self.registers[op.c] = if op.a == self.registers[op.b] { 1 } else { 0 },
      EQRI => self.registers[op.c] = if self.registers[op.a] == op.b { 1 } else { 0 },
      EQRR => self.registers[op.c] = if self.registers[op.a] == self.registers[op.b] { 1 } else { 0 },
      _ => panic!("opcode {} not supported", op.code),
    }

    self.registers[self.ip] += 1;
    if self.registers[self.ip] >= self.ops.len() {
      true
    } else {
      false
    }
  }

  pub fn exec_until_loop(&mut self) -> bool {
    let mut seen_once = vec![false; self.ops.len()];
    let mut seen_twice = vec![false; self.ops.len()];

    loop {
      if self.exec() {
        return true;
      }

      let ic = self.registers[self.ip];

      if seen_once[ic] {
        if seen_twice[ic] {
          return false;
        } else {
          seen_twice[ic] = true;
        }
      } else {
        seen_once[ic] = true;
      }
    }
  }

  pub fn parse(input: &str) -> Program {
    let mut ops: Vec<Op> = Vec::with_capacity(128);
    let mut ip: usize = 0;

    for line in input.lines() {
      let op = Op::parse(line);

      if op.code == IP {
        ip = op.a;
        continue;
      }

      ops.push(op);
    }

    Program{
      registers: vec![0usize; 6],
      ip, ops,
    }
  }
}

pub fn part1(input: &str) {
  let mut program = Program::parse(input);
  
  if program.exec_until_loop() {
    println!("Results: {}", program.registers[0]);
    return;
  }

  let seed = *program.registers.iter().max().unwrap();
  let mut total = 0;

  for i in 1..=seed {
    if seed % i == 0 {
        total += i;
    }
  }

  println!("Result: {}", total);
}

pub fn part2(input: &str) {
  let mut program = Program::parse(input);
  program.registers[0] = 1;
  
  if program.exec_until_loop() {
    println!("Results: {}", program.registers[0]);
    return;
  }

  let target = *program.registers.iter().max().unwrap();
  let mut total = 0;

  for i in 1..=target {
    if target % i == 0 {
      println!("{}", i);

      total += i;
    }
  }

  println!("Result: {}", total);
}