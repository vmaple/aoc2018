pub fn part1(input: &str) {
    let sum: i32 = input.lines()
                        .map(|l| l.parse::<i32>())
                        .filter(|v| v.is_ok())
                        .map(|v| v.unwrap())
                        .sum();

    println!("Result: {}", sum);
}

pub fn part2(input: &str) {
    let mut line = [0u8; 1000000];
    let mut current_sum: i32 = 0;
    let nums: Vec<i32> = input.lines().map(|l| l.parse::<i32>().unwrap_or(0)).collect();

    for n in nums.iter().cycle() {
        current_sum += n;
        let pos = (current_sum + 500000) as usize;

        unsafe {
            if *line.get_unchecked(pos) == 1 {
                break;
            }
            *line.get_unchecked_mut(pos) = 1;
        }
    }

    println!("Result: {}", current_sum);
}