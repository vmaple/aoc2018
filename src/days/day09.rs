use std::mem;
use std::alloc;
use std::ptr;

/// EvilCirlce is a deque that trusts the user to never go beyond the cap in either direction.
struct EvilCirlce {
    root: *mut u32,
    front: isize,
    back: isize,
}

impl EvilCirlce {
    #[inline]
    pub fn push_front(&mut self, num: u32) {     
        self.front -= 1;

        unsafe {
            ptr::write(self.root.offset(self.front), num);
        }
    }

    #[inline]
    pub fn pop_front(&mut self) -> u32 {
        self.front += 1;

        unsafe {
            return ptr::read(self.root.offset(self.front - 1));
        }
    }

    #[inline]
    pub fn rotate_left(&mut self, count: isize) {
        unsafe {
            self.front -= count;
            self.back -= count;
            ptr::copy(self.root.offset(self.back + 1), self.root.offset(self.front), count as usize);
        }
    }

    #[inline]
    pub fn rotate_right(&mut self, count: isize) {
        if self.back == self.front {
            return
        }

        unsafe {
            for _ in 0..count {
                self.back += 1;
                ptr::write(self.root.offset(self.back), ptr::read(self.root.offset(self.front)));
                self.front += 1;
            }
        }
    }

    pub fn new(cap: usize) -> EvilCirlce {
        let align = mem::align_of::<u32>();
        let elem_size = mem::size_of::<u32>();

        unsafe {
            let ptr = alloc::alloc(alloc::Layout::from_size_align(elem_size * cap * 2, align).unwrap());
            if ptr.is_null() {
                panic!("Failed to allocate EvilCirlce");
            }

            return EvilCirlce{
                root: ptr as *mut u32,
                front: cap as isize + 1,
                back: cap as isize,
            };
        }
    }
}

fn highest_score(player_count: usize, last_marble: u32) -> u32 {
    let mut marbles = EvilCirlce::new(last_marble as usize * 3);
    let mut scores = vec![0; last_marble as usize];

    marbles.push_front(0);

    for next_marble in 1..last_marble+1 {
        if next_marble % 23 == 0 {
            marbles.rotate_left(7);
            scores[next_marble as usize % player_count] += next_marble + marbles.pop_front();
        } else {
            marbles.rotate_right(2);
            marbles.push_front(next_marble);
        }
    }

    *scores.iter().max().unwrap()
}

pub fn part1(input: &str) {
    for line in input.lines() {
        let split: Vec<u32> = line.split(' ').map(|t| t.parse::<u32>()).filter(|o| o.is_ok()).map(|o| o.unwrap()).collect();
        let player_count = *split.get(0).unwrap() as usize;
        let last_marble = *split.get(1).unwrap();

        println!("Result: {}", highest_score(player_count, last_marble))
    }
}

pub fn part2(input: &str) {
    for line in input.lines() {
        let split: Vec<u32> = line.split(' ').map(|t| t.parse::<u32>()).filter(|o| o.is_ok()).map(|o| o.unwrap()).collect();
        let player_count = *split.get(0).unwrap() as usize;
        let last_marble = *split.get(1).unwrap();

        println!("Result: {}", highest_score(player_count, last_marble * 100))
    }
}