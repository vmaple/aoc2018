pub fn part1(input: &str) {
	let mut first: u32 = 10;
	let mut prev: u32 = 10;
	let mut sum: u32 = 0;

    for ch in input.chars() {
		let n = ch.to_digit(10);
		if let Some(n) = n {
			if n == prev {
				sum += n;
			}

			prev = n;

			if first == 10 {
				first = n;
			}
		} else {
			eprintln!("Failed to parse '{}' as digit", ch);
		}
	}

	// If the first and last are the same, that's a match that should be added.
	if first == prev {
		sum += first;
	}

	println!("Result: {}", sum);
}