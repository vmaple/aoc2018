use crate::common::rectangle::Rectangle;

struct Claim {
    id: String,
    rect: Rectangle,
}

pub fn part1(input: &str) {
    let claims: Vec<Claim> = input.lines().map(|line| {
        let split: Vec<&str> = line.split(' ').collect();
        
        Claim{
            id: String::from(split[0]),
            rect: Rectangle::parse_separate(split[2], split[3]).unwrap(),
        }
    }).collect();

    let mut grid = [[0u32; 1000]; 1000];

    for claim in claims.iter() {
        for x in claim.rect.x..(claim.rect.x + claim.rect.w) {
            for y in claim.rect.y..(claim.rect.y + claim.rect.h) {
                grid[x as usize][y as usize] += 1;
            }
        }
    }

    let mut count = 0;
    for row in grid.iter() {
        for col in row.iter() {
            if *col > 1 {
                count += 1;
            }
        }
    }

    println!("Result: {}", count);
}

pub fn part2(input: &str) {
    let claims: Vec<Claim> = input.lines().map(|line| {
        let split: Vec<&str> = line.split(' ').collect();
        
        Claim{
            id: String::from(split[0]),
            rect: Rectangle::parse_separate(split[2], split[3]).unwrap(),
        }
    }).collect();

    let mut grid = [[0u32; 1000]; 1000];

    for claim in claims.iter() {
        for x in claim.rect.x..(claim.rect.x + claim.rect.w) {
            for y in claim.rect.y..(claim.rect.y + claim.rect.h) {
                grid[x as usize][y as usize] += 1;
            }
        }
    }

    for claim in claims.iter() {
        let mut failed = false;

        for x in claim.rect.x..(claim.rect.x + claim.rect.w) {
            for y in claim.rect.y..(claim.rect.y + claim.rect.h) {
                if grid[x as usize][y as usize] > 1 {
                    failed = true;
                    break;
                }
            }

            if failed {
                break;
            }
        }

        if !failed {
            println!("Result: {}", claim.id);
        }
    }
}