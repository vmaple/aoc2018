const CHAR_A: usize = 'A' as usize;

pub fn part1(input: &str) {
    let mut depends: Vec<Vec<usize>> = Vec::with_capacity(26);
    let mut result: Vec<char> = Vec::with_capacity(input.len());
    let mut done = [false; 26];
    let mut max_letter = 0;

    for _ in 0..26 {
        depends.push(Vec::with_capacity(26));
    }
    let depends: &mut [Vec<usize>] = &mut depends;

    for line in input.lines() {
        let split: Vec<char> = line.split(' ').filter(|t| t.len() == 1).map(|t| t.chars().next().unwrap()).collect();
        let dependency = *split.get(0).unwrap() as usize - CHAR_A;
        let dependent = *split.get(1).unwrap() as usize - CHAR_A;

        if dependent > max_letter {
            max_letter = dependent;
        }
        if dependency > max_letter {
            max_letter = dependency;
        }

        depends[dependent].push(dependency);
    }

    while result.len() <= max_letter {
        for i in 0..max_letter+1 {
            if done[i] {
                continue;
            }

            let mut not_satisfied = false;

            for dep in depends[i].iter() {
                if !done[*dep] {
                    not_satisfied = true;
                    break;
                }
            }

            if !not_satisfied {
                result.push((i + CHAR_A) as u8 as char);
                done[i] = true;
                break;
            }
        }
    }

    println!("Result: {}", result.iter().collect::<String>());
}

fn get_woker_count(max_letter: usize) -> usize {
    if max_letter > 16 {
        5
    } else {
        2
    }
}

fn get_min_time(max_letter: usize) -> usize {
    if max_letter > 16 {
        61
    } else {
        1
    }
}


pub fn part2(input: &str) {
    let mut depends: Vec<Vec<usize>> = Vec::with_capacity(26);
    let mut done = [false; 26];
    let mut max_letter = 0;

    for _ in 0..26 {
        depends.push(Vec::with_capacity(26));
    }
    let depends: &mut [Vec<usize>] = &mut depends;

    for line in input.lines() {
        let split: Vec<char> = line.split(' ').filter(|t| t.len() == 1).map(|t| t.chars().next().unwrap()).collect();
        let dependency = *split.get(0).unwrap() as usize - CHAR_A;
        let dependent = *split.get(1).unwrap() as usize - CHAR_A;

        if dependent > max_letter {
            max_letter = dependent;
        }
        if dependency > max_letter {
            max_letter = dependency;
        }

        depends[dependent].push(dependency);
    }

    let worker_count = get_woker_count(max_letter);
    let minimum_time = get_min_time(max_letter);
    let mut second = -1;
    let mut workers = Vec::with_capacity(worker_count);
    let mut working_on = [27usize; 5];
    let mut reserved = [false; 26];
    let mut done_count = 0;
    for _i in 0..worker_count {
        workers.push(0);
    }
    let workers: &mut [usize] = &mut workers;
    let mut free_worker;

    while done_count <= max_letter {
        second += 1;

        free_worker = workers.len();
        for i in 0..worker_count {
            if workers[i] > 0 {
                workers[i] -= 1;

                if workers[i] == 0 {
                    done[working_on[i]] = true;
                    done_count += 1;
                }
            } else {
                free_worker = i;
            }
        }
        if free_worker == workers.len() {
            continue;
        }

        for i in 0..max_letter+1 {
            if done[i] || reserved[i] {
                continue;
            }

            let mut satisfied = true;

            for dep in depends[i].iter() {
                if !done[*dep] {
                    satisfied = false;
                    break;
                }
            }

            if satisfied {
                free_worker = workers.len();
                for i in 0..worker_count {
                    if workers[i] == 0 {
                        free_worker = i;
                        break;
                    }
                }
                if free_worker == workers.len() {
                    break;
                }

                reserved[i] = true;
                working_on[free_worker] = i;
                workers[free_worker] = i + minimum_time;
            }
        }
    }

    println!("Result: {}", second);
}