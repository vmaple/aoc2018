pub fn part1(input: &str) {
	let input: usize = input.parse().unwrap();
	let target = input + 10;
	let mut data: Vec<u8> = Vec::with_capacity(target + 2);
	let mut elf_1 = 0;
	let mut elf_2 = 1;

	data.push(3);
	data.push(7);

	while data.len() < target {
		let value_1 = unsafe { *data.get_unchecked(elf_1) };
		let value_2 = unsafe { *data.get_unchecked(elf_2) };
		let sum = value_1 + value_2;

		if sum >= 10 {
			data.push(1);
			data.push(sum % 10);
		} else {
			data.push(sum);
		}

		elf_1 = (elf_1 + (value_1 as usize) + 1) % data.len();
		elf_2 = (elf_2 + (value_2 as usize) + 1) % data.len();
	}

	let mut score: usize = 0;
	for i in input..target {
		score = (score * 10) + (data[i] as usize);
	}

	println!("Scores: {}", score);
}

const TEN: usize = 10;

pub fn part2(input: &str) {
	let digits = input.len();
	let input: usize = input.parse().unwrap();
	let mut data: Vec<u8> = Vec::with_capacity(1024 * 1024 * 64);
	let mut elf_1 = 0;
	let mut elf_2 = 1;

	let max = TEN.pow(digits as u32);
	let offset = digits;

	let mut score = 37;
	data.push(3);
	data.push(7);

	loop {
		let value_1 = unsafe { *data.get_unchecked(elf_1) as usize };
		let value_2 = unsafe { *data.get_unchecked(elf_2) as usize };
		let sum = value_1 + value_2;

		if sum >= 10 {
			data.push(1);
			score = ((score * 10) + 1) % max;
			if score == input {
				println!("Steps: {}", data.len() - offset);

				return;
			}

			data.push((sum % 10) as u8);
			score = ((score * 10) + (sum % 10)) % max;
		} else {
			data.push(sum as u8);
			score = ((score * 10) + sum) % max;
		}

		elf_1 = (elf_1 + value_1 + 1) % data.len();
		elf_2 = (elf_2 + value_2 + 1) % data.len();

		if score == input {
			println!("Steps: {}", data.len() - offset);

			return;
		}
	}
}