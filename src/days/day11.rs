pub fn power_level(sn: i32, x: i32, y: i32) -> i32 {
    let rack_id = x + 10;

    (((((rack_id * y) + sn) * rack_id) / 100) % 10) - 5
}

pub fn part1(input: &str) {
    let sn = input.parse::<i32>().unwrap();
    let mut winner_x = 0;
    let mut winner_y = 0;
    let mut winner_power = 0;

    let mut power_levels = [0i32; 300*300];
    for y in 1..=300 {
        for x in 1..=300 {
            let above = get_value(&power_levels, x, y - 1);
            let left = get_value(&power_levels, x - 1, y);
            let aboveleft = get_value(&power_levels, x - 1, y - 1);

            unsafe {
                *power_levels.get_unchecked_mut(get_index(x, y)) = power_level(sn, x, y) + above + left - aboveleft;
            }
        }
    }

    for x in 3..=300 {
        for y in 3..=300 {
            let power = get_value(&power_levels, x, y) - get_value(&power_levels, x, y - 3) - get_value(&power_levels, x - 3, y) + get_value(&power_levels, x - 3, y - 3);

            if power > winner_power {
                winner_x = x;
                winner_y = y;
                winner_power = power;
            }
        }
    }

    println!("Result: {},{}", winner_x - 2, winner_y - 2);
}

#[inline]
fn get_index(x: i32, y: i32) -> usize {
    ((y - 1) * 300 + (x - 1)) as usize
}

#[inline]
fn get_value(levels: &[i32], x: i32, y: i32) -> i32 {
    if x < 1 || y < 1 {
        0
    } else {
        unsafe {
            return *levels.get_unchecked(get_index(x, y));
        }
    }
}

pub fn part2(input: &str) {
    let sn = input.parse::<i32>().unwrap();
    let mut winner_x = 0;
    let mut winner_y = 0;
    let mut winner_w = 0;
    let mut winner_power = 0;

    let mut power_levels = [0i32; 300*300];
    for y in 1..=300 {
        for x in 1..=300 {
            let above = get_value(&power_levels, x, y - 1);
            let left = get_value(&power_levels, x - 1, y);
            let aboveleft = get_value(&power_levels, x - 1, y - 1);

            unsafe {
                *power_levels.get_unchecked_mut(get_index(x, y)) = power_level(sn, x, y) + above + left - aboveleft;
            }
        }
    }

    for w in 1..=300 {
        for y in w..=300 {
            for x in w..=300 {
                let power = get_value(&power_levels, x, y) - get_value(&power_levels, x, y - w) - get_value(&power_levels, x - w, y) + get_value(&power_levels, x - w, y - w);

                if power > winner_power {
                    winner_x = x;
                    winner_y = y;
                    winner_w = w;
                    winner_power = power;
                }
            }
        }
    }

    println!("Result: {},{},{}", winner_x - winner_w + 1, winner_y - winner_w + 1, winner_w);
}