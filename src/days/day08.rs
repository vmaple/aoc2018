struct Node<'a> {
    metadata: &'a [u32],
    children: Vec<Node<'a>>,
    size: usize,
}

impl<'a> Node<'a> {
    fn value(&self) -> u32 {
        if self.children.len() == 0 {
            self.metadata.iter().sum()
        } else {
            self.metadata.iter().map(|n| self.children.get((*n - 1) as usize)).filter(|c| c.is_some()).map(|c| c.unwrap().value()).sum()
        }
    }

    fn sum(&self) -> u32 {
        let node_sum: u32 = self.metadata.iter().cloned().sum();
        let child_sum: u32 = self.children.iter().map(|c| c.sum()).sum();
        
        node_sum + child_sum
    }

    fn parse(data: &'a [u32]) -> Node<'a> {
        let child_len = data[0] as usize;
        let meta_len = data[1] as usize;
        let mut size = 2;
        let mut children = Vec::new();

        for _ in 0..child_len {
            let child = Node::parse(&data[size..]);
            size += child.size;

            children.push(child);
        }

        let metadata = &data[size..size+meta_len];

        Node{
            metadata: metadata,
            children: children,
            size: size + meta_len,
        }
    }
}

pub fn part1(input: &str) {
    let numbers: Vec<u32> = input.split(' ').map(|t| t.parse().unwrap_or(0)).collect();

    println!("Result: {}", Node::parse(&numbers).sum());
}

pub fn part2(input: &str) {
    let numbers: Vec<u32> = input.split(' ').map(|t| t.parse().unwrap_or(0)).collect();

    println!("Result: {}", Node::parse(&numbers).value());
}

pub fn combined(input: &str) {
    let numbers: Vec<u32> = input.split(' ').map(|t| t.parse().unwrap_or(0)).collect();
    let root = Node::parse(&numbers);

    println!("P1: {}", root.sum());
    println!("P2: {}", root.value());
}