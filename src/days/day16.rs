const ADDR: usize = 0;
const ADDI: usize = 1;
const MULR: usize = 2;
const MULI: usize = 3;
const BANR: usize = 4;
const BANI: usize = 5;
const BORI: usize = 6;
const BORR: usize = 7;
const SETR: usize = 8;
const SETI: usize = 9;
const GTIR: usize = 10;
const GTRI: usize = 11;
const GTRR: usize = 12;
const EQIR: usize = 13;
const EQRI: usize = 14;
const EQRR: usize = 15;
const UNKNOWN: usize = 16;

const CH_ZERO: usize = '0' as usize;

fn permissive_parse(s: &str) -> usize {
  let mut acc = 0;

  for ch in s.chars() {
    match ch {
      '0'...'9' => {acc = (acc * 10) + (ch as usize - CH_ZERO)},
      _ => {},
    }
  }

  acc
}

struct Program {
  op_map: Vec<usize>,
  i_map: Vec<usize>,
  remaining: usize,
}

impl Program {
  fn count_matches(&self, sample: &Sample) -> usize {
    let mut registers: Vec<usize> = sample.before.iter().cloned().collect();
    let mut op: Vec<usize> = sample.op.iter().cloned().collect();
    let mut matches = 0;
    let change_index = op[3];

    let max = if sample.after[change_index] < 2 { 16 } else { 10 };
    for i_code in 0..max {
      op[0] = i_code;

      self.exec(false, &op, &mut registers);
      if registers[change_index] == sample.after[change_index] {
        matches += 1;
      }
      registers[change_index] = sample.before[change_index];
    }

    matches
  }

  fn probe(&mut self, sample: &Sample) -> bool {
    let mut registers: Vec<usize> = sample.before.iter().cloned().collect();
    let mut op: Vec<usize> = sample.op.iter().cloned().collect();
    let mut candidate = UNKNOWN;
    let op_code = op[0];
    let change_index = op[3];

    let max = if sample.after[change_index] < 2 { 16 } else { 10 };
    for i_code in 0..max {
      if self.i_map[i_code] != UNKNOWN {
        continue;
      }

      op[0] = i_code;

      self.exec(false, &op, &mut registers);
      if registers[change_index] == sample.after[change_index] {
        if candidate == UNKNOWN {
          candidate = i_code;
        } else {
          return false;
        }
      }
      registers[change_index] = sample.before[change_index];
    }

    if candidate != UNKNOWN {
      self.op_map[op_code] = candidate;
      self.i_map[candidate] = op_code;
      self.remaining -= 1;

      true
    } else {
      false
    }
  }

  fn run(&mut self, op: &[usize], registers: &mut [usize]) {
    self.exec(true, op, registers);
  }

  fn exec(&self, mapped: bool, op: &[usize], registers: &mut [usize]) {
    let i_code = if mapped { self.op_map[op[0]] } else { op[0] };
    let a = op[1];
    let b = op[2];
    let c = op[3];

    match i_code {
      ADDR => registers[c] = registers[a] + registers[b],
      ADDI => registers[c] = registers[a] + b,
      MULR => registers[c] = registers[a] * registers[b],
      MULI => registers[c] = registers[a] * b,
      BANR => registers[c] = registers[a] & registers[b],
      BANI => registers[c] = registers[a] & b,
      BORR => registers[c] = registers[a] | registers[b],
      BORI => registers[c] = registers[a] | b,
      SETR => registers[c] = registers[a],
      SETI => registers[c] = a,
      GTIR => registers[c] = if a > registers[b] { 1 } else { 0 },
      GTRI => registers[c] = if registers[a] > b { 1 } else { 0 },
      GTRR => registers[c] = if registers[a] > registers[b] { 1 } else { 0 },
      EQIR => registers[c] = if a == registers[b] { 1 } else { 0 },
      EQRI => registers[c] = if registers[a] == b { 1 } else { 0 },
      EQRR => registers[c] = if registers[a] == registers[b] { 1 } else { 0 },
      _ => panic!("opcode {} not supported", i_code),
    }
  }

  pub fn new() -> Program {
    Program{
      op_map: vec![UNKNOWN; 16],
      i_map: vec![UNKNOWN; 16],
      remaining: 16,
    }
  }
}

struct Sample {
  before: Vec<usize>,
  op: Vec<usize>,
  after: Vec<usize>,
}

impl Sample {
  fn parse(before_line: &str, op_line: &str, after_line: &str) -> Sample {
    let mut before = Vec::with_capacity(4);
    let mut op = Vec::with_capacity(4);
    let mut after = Vec::with_capacity(4);
    
    for n in before_line.split(' ').skip(1).map(|t| permissive_parse(t)) {
      before.push(n);
    }
    for n in op_line.split(' ').map(|t| permissive_parse(t)) {
      op.push(n);
    }
    for n in after_line.split(' ').skip(2).map(|t| permissive_parse(t)) {
      after.push(n);
    }

    Sample{before, op, after}
  }
}

pub fn part1(input: &str) {
  let program = Program::new();
  let lines = &mut input.lines();
  let mut count = 0;

  loop {
    let before_line = lines.next().unwrap_or("");
    if before_line.len() == 0 {
      break;
    }
    let op_line = lines.next().unwrap();
    let after_line = lines.next().unwrap();

    let sample = Sample::parse(before_line, op_line, after_line);

    if program.count_matches(&sample) >= 3 {
      count += 1;
    }

    lines.next();
  }

  println!("Result: {}", count);
}

pub fn part2(input: &str) {
  let mut program = Program::new();
  let mut registers: Vec<usize> = vec![0; 4];
  let lines = &mut input.lines();

  // Parse samples.
  loop {
    let before_line = lines.next().unwrap_or("");
    if before_line.len() == 0 {
      break;
    }
    let op_line = lines.next().unwrap();
    let after_line = lines.next().unwrap();

    if program.remaining > 0 {
      let sample = Sample::parse(before_line, op_line, after_line);
      program.probe(&sample);
    }

    lines.next();
  }

  // Sanity check
  for n in program.op_map.iter() {
    if *n == UNKNOWN {
      panic!("op code {} is still unkown", n);
    }
  }

  // Run the program
  let mut op: Vec<usize> = Vec::with_capacity(4);
  for line in lines.skip_while(|l| l.len() == 0) {
    op.clear();
    for n in line.split(' ').map(|t| permissive_parse(t)) {
      op.push(n);
    }

    program.run(&op, &mut registers);
  }

  println!("Result: {}", registers[0]);
}