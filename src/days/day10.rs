use crate::common::interator::Interator;

struct Star {
    x: i32,
    y: i32,
    vx: i32,
    vy: i32,
}

impl Star {
    #[inline]
    pub fn pos_at(&self, sec: i32) -> (i32, i32) {
        (self.x + (self.vx * sec), self.y + (self.vy * sec))
    }

    pub fn time_until_in_frame(&self, width: i32, height: i32) -> i32 {
        let mut time = 0;

        if self.x >= width {
            let duration = (self.x - width) / -self.vx;
            if duration > time {
                time = duration;
            }
        } else if self.x < 0 {
            let duration = -self.x / self.vx;
            if duration > time {
                time = duration;
            }
        }

        if self.y >= height {
            let duration = (self.y - width) / -self.vy;
            if duration > time {
                time = duration;
            }
        } else if self.y < 0 {
            let duration = -self.y / self.vy;
            if duration > time {
                time = duration;
            }
        }

        time
    }

    pub fn parse(line: &str) -> Star {
        let mut iter = Interator::from_iter(line.chars());

        let x = iter.next().unwrap();
        let y = iter.next().unwrap();
        let vx = iter.next().unwrap();
        let vy = iter.next().unwrap();

        Star { x, y, vx, vy }
    }
}

fn print_grid(grid: &[char], width: usize, height: usize) {
    let mut iter = grid.iter();
    
    for _ in 0..height {
        println!("{}", (&mut iter).take(width).collect::<String>());
    }
}

fn get_height_goal(stars_len: usize) -> usize {
    if stars_len > 32 {
        10
    } else {
        8
    }
}

pub fn combined(input: &str) {
    let stars: Vec<Star> = input.lines().map(Star::parse).collect();
    let goal = get_height_goal(stars.len());
    let mut time_until_last_star = 0;
    for star in stars.iter().take(8) {
        let time = star.time_until_in_frame(384, 256);
        if time > time_until_last_star {
            time_until_last_star = time;
        }
    }

    for sec in time_until_last_star.. {
        let mut exceeds_goal = false;
        let mut min_x = 384;
        let mut max_x = 0;
        let mut min_y = 256;
        let mut max_y = 0;

        for star in stars.iter() {
            let (x, y) = star.pos_at(sec);

            if x < min_x {
                min_x = x;
            } else if x > max_x {
                max_x = x;
            }
            if y < min_y {
                min_y = y;
            } else if y > max_y {
                max_y = y;

                if ((max_y - min_y) + 1) > goal as i32 {
                    exceeds_goal = true;
                    break;
                }
            }
        }

        if exceeds_goal {            
            continue;
        }

        let height = 1 + (max_y - min_y) as usize;
        let width = 1 + (max_x - min_x) as usize;

        if height == goal {
            let mut grid = vec!['.'; width * height];
            for star in stars.iter() {
                let (x, y) = star.pos_at(sec);
                let index = ((y - min_y) as usize * width) + (x - min_x) as usize;

                grid[index] = '#';
            }

            println!("Second: {}", sec);
            print_grid(&grid, width, height);
            break;
        }
    }
}