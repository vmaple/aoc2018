pub fn part1(input: &str) {
	let mut lines = input.lines();
	let mut grows = [false; 33];

	let mut states = vec!['.'; 32];
	for ch in (&mut lines).next().unwrap().chars().skip_while(|c| *c != ':').skip(1){
		states.push(ch);
	}

	// Parse rules
	for line in (&mut lines).skip(1) {
		let mut chars = line.chars();
		let mut index = 0;

		for (i, ch) in (&mut chars).take(5).enumerate() {
			if ch == '#' {
				index = index | (1 << i);
			}
		}

		if (&mut chars).skip(4).next().unwrap() == '#' {
			grows[index] = true;
		} else {
			grows[index] = false;
		}
	}

	let grows_0 = if grows[0] {'#'} else {'.'};
	let mut current: Vec<char> = Vec::with_capacity(states.len() + 20 + 4);

	// Handle plants
	for _ in 0..20 {
		current.clear();

		let mut start = 0;
		for i in 0..states.len() {
			if unsafe { *states.get_unchecked(i) == '#' } {
				start = i - 2;
				current.pop();
				current.pop();
				break;
			}

			current.push(grows_0);
		}


		let len = states.len()+1;
		for i in start..len {
			let mut index = 0;
			for j in 0..5 {
				if j < 2 && i < 2 - j {
					continue;
				}

				if let Some(ch) = states.get(i + j - 2) {
					if *ch == '#' {
						index = index | (1 << j);
					}
				}
			}

			if unsafe { *grows.get_unchecked(index) } {
				current.push('#');
			} else {
				current.push('.');
			}
		}

		states.clear();
		for ch in current.iter().take(len) {
			states.push(*ch);
		}
	}

	let mut score = 0;
	for (i, ch) in states.iter().enumerate() {
		if *ch == '#' {
			score += (i as i64) - 33;
		}
	}

	println!("Score: {}", score);
}

pub fn part2(input: &str) {
	let mut lines = input.lines();
	let mut grows = [false; 33];

	let mut states = vec!['.'; 32];
	for ch in (&mut lines).next().unwrap().chars().skip_while(|c| *c != ':').skip(1){
		states.push(ch);
	}

	// Parse rules
	for line in (&mut lines).skip(1) {
		let mut chars = line.chars();
		let mut index = 0;

		for (i, ch) in (&mut chars).take(5).enumerate() {
			if ch == '#' {
				index = index | (1 << i);
			}
		}

		if (&mut chars).skip(4).next().unwrap() == '#' {
			grows[index] = true;
		} else {
			grows[index] = false;
		}
	}

	let grows_0 = if grows[0] {'#'} else {'.'};

	let mut prev_score = 0;
	let mut prev_diff = 0;
	let mut prev_prev_diff = 0;
	let mut current: Vec<char> = Vec::with_capacity(2048);

	// Handle plants
	for n in 1.. {
		let mut score = 0;
		let len = states.len() + 1;

		current.clear();

		let mut start = 0;
		for i in 0..len {
			if unsafe { *states.get_unchecked(i + 2) == '#' } {
				start = i;
				break;
			}

			current.push(grows_0);
		}
		
		let mut last_plant = 0;

		for i in start..len {
			let mut index = 0;
			for j in 0..5 {
				if j < 2 && i < 2 - j {
					continue;
				}

				if unsafe { *states.get_unchecked(i + j - 2)  == '#' } {
					index = index | (1 << j);
				}
			}

			if unsafe { *grows.get_unchecked(index) } {
				score += (i as i64) - 33;
				current.push('#');

				last_plant = current.len();
			} else {
				current.push('.');
			}
		}

		let diff = score - prev_score;
		if diff == prev_diff && diff == prev_prev_diff {
			let score = (score as i64) + ((diff as i64) * (50000000000 - n as i64));
			println!("Score: {}", score);
			return;
		}
		prev_score = score;
		prev_prev_diff = prev_diff;
		prev_diff = diff;

		states.clear();
		for ch in current.iter().take(last_plant) {
			states.push(*ch);
		}
	}
}