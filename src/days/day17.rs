use std::cmp::{Eq, PartialEq};

#[derive(Debug, Eq, PartialEq)]
enum RangeAxis {
  X, Y
}

#[derive(Debug)]
struct Range {
  x: usize,
  y: usize,
  axis: RangeAxis,
  max: usize,
}

impl Range {
  pub fn positions(&self) -> RangeIterator {
    RangeIterator{
      range: &self,
      index: 0,
    }
  }

  pub fn parse(line: &str) -> Range {
    let split = &mut line.split(", ");
    let left = split.next().unwrap();
    let right = split.next().unwrap();

    let left_value: usize = (&left[2..]).parse().unwrap();

    let right_axis = right.chars().next().unwrap();
    let right_split = &mut (&right[2..]).split('.');

    let right_value_1: usize = right_split.next().unwrap().parse().unwrap();
    let right_value_2: usize = right_split.skip(1).next().unwrap().parse().unwrap();
    
    if right_axis == 'y' {
      Range{
        x: left_value,
        y: right_value_1,
        axis: RangeAxis::Y,
        max: right_value_2 - right_value_1,
      }
    } else {
      Range{
        x: right_value_1,
        y: left_value,
        axis: RangeAxis::X,
        max: right_value_2 - right_value_1,
      }
    }
  }
}

struct RangeIterator<'a> {
  range: &'a Range,
  index: usize,
}

impl<'a> Iterator for RangeIterator<'a> {
  type Item = (usize, usize);

  fn next(&mut self) -> Option<(usize, usize)> {
    if self.index <= self.range.max {
      let index = self.index;
      self.index += 1;

      if self.range.axis == RangeAxis::X {
        Some((self.range.x + index, self.range.y))
      } else {
        Some((self.range.x, self.range.y + index))
      }
    } else {
      None
    }
  }
}

struct Board {
  min_x: usize,
  min_y: usize,
  max_x: usize,
  max_y: usize,
  width: usize,
  data: Vec<char>,
}

impl Board {
  pub fn print(&self) {
    let width = (self.max_x - self.min_x) + 1;

    println!("({},{}) to ({}, {})", self.min_x, self.min_y, self.max_x, self.max_y);
    for y in self.min_y..=self.max_y {
      for x in self.min_x..=self.max_x {
        let index = (y * self.width) + (x-self.min_x);

        print!("{}", self.data[index]);
      }

      print!("\n");
    }
  }

  fn get_position(&self, index: usize) -> usize {
    (index / self.width) + (index % self.width)
  }

  fn tick(&mut self) -> bool {
    let mut changed = false;

    for y in self.min_y..=self.max_y {
      for x in self.min_x..=self.max_x {
        let current = (y * self.width) + (x-self.min_x);
        let left = (y * self.width) + ((x-1)-self.min_x);
        let right = (y * self.width) + ((x+1)-self.min_x);
        let below = ((y+1) * self.width) + (x-self.min_x);

        if self.data[current] == '|' {
          if y != self.max_y {
            if self.data[below] != '~' {
              self.data[below] = '|';
              changed = true;
            } else if self.data[below] == '#' && self.data[left] == '#' && self.data[right] == '|' {
              self.data[current] = '~';
              changed = true;
            } else if self.data[below] == '#' && self.data[right] == '#' && self.data[left] == '|' {
              self.data[current] = '~';
              changed = true;
            } else if self.data[left] == '~' || self.data[right] == '~' {
              self.data[current] = '~';
              changed = true;
            }
          }
        }
      }
    }

    changed
  }

  pub fn make_board(&mut self, ranges: &[Range]) {
    let mut first = true;

    for range in ranges.iter() {
      for (x, y) in range.positions() {
        if !first {
          if x < self.min_x {
            self.min_x = x;
          }
          if y < self.min_y {
            self.min_y = y;
          }
          if x > self.max_x {
            self.max_x = x;
          }
          if y > self.max_y {
            self.max_y = y;
          }
        } else {
          self.min_x = x;
          self.min_y = y;
          self.max_x = x;
          self.max_y = y;

          first = false;
        }
      }
    }

    let width = (self.max_x - self.min_x) + 1;
    let height = self.max_y + 1;

    self.data = vec!['.'; width * height];

    for range in ranges.iter() {
      for (x, y) in range.positions() {
        let index = ((y-self.min_y) * width) + (x-self.min_x);

        self.data[index] = '#';
      }
    }

    self.width = width;
  }

  pub fn from_ranges(ranges: &[Range]) -> Board {
    let mut board = Self::new();
    board.make_board(ranges);

    board
  }

  pub fn new() -> Board {
    Board{
      min_x: 0,
      min_y: 0,
      max_x: 0,
      max_y: 0,
      width: 0,
      data: Vec::new(),
    }
  }
}

pub fn part1(input: &str) {
  let ranges: Vec<Range> = input.lines().map(Range::parse).collect(); 
  let mut board: Board = Board::from_ranges(&ranges);

  loop {
    board.tick();
    board.print();
  }
}

pub fn part2(input: &str) {
  let _input = input;
}