#[macro_use]
extern crate lazy_static;

use chrono::prelude::*;
use std::fs::File;
use std::io::Read;
use std::env;
use std::process;

mod aoc;
mod days;
mod common;

fn main() {
    // Register days here.
    aoc::register_day_handler(30, 1, days::day30::part1);
    aoc::register_day_handler(1, 1, days::day01::part1);
    aoc::register_day_handler(1, 2, days::day01::part2);
    aoc::register_day_handler(2, 1, days::day02::part1);
    aoc::register_day_handler(2, 2, days::day02::part2);
    aoc::register_day_handler(3, 1, days::day03::part1);
    aoc::register_day_handler(3, 2, days::day03::part2);
    aoc::register_day_handler(4, 1, days::day04::both_parts);
    aoc::register_day_handler(4, 2, days::day04::both_parts);
    aoc::register_day_handler(5, 1, days::day05::part1);
    aoc::register_day_handler(5, 2, days::day05::part2);
    aoc::register_day_handler(6, 1, days::day06::part1);
    aoc::register_day_handler(6, 2, days::day06::part2);
    aoc::register_day_handler(7, 1, days::day07::part1);
    aoc::register_day_handler(7, 2, days::day07::part2);
    aoc::register_day_handler(8, 1, days::day08::part1);
    aoc::register_day_handler(8, 2, days::day08::part2);
    aoc::register_day_handler(8, 3, days::day08::combined);
    aoc::register_day_handler(9, 1, days::day09::part1);
    aoc::register_day_handler(9, 2, days::day09::part2);
    aoc::register_day_handler(10, 1, days::day10::combined);
    aoc::register_day_handler(10, 2, days::day10::combined);
    aoc::register_day_handler(11, 1, days::day11::part1);
    aoc::register_day_handler(11, 2, days::day11::part2);
    aoc::register_day_handler(12, 1, days::day12::part1);
    aoc::register_day_handler(12, 2, days::day12::part2);
    aoc::register_day_handler(13, 1, days::day13::part1);
    aoc::register_day_handler(13, 2, days::day13::part2);
    aoc::register_day_handler(13, 3, days::day13::combined);
    aoc::register_day_handler(14, 1, days::day14::part1);
    aoc::register_day_handler(14, 2, days::day14::part2);
    aoc::register_day_handler(16, 1, days::day16::part1);
    aoc::register_day_handler(16, 2, days::day16::part2);
    aoc::register_day_handler(18, 1, days::day18::part1);
    aoc::register_day_handler(18, 2, days::day18::part2);
    aoc::register_day_handler(19, 1, days::day19::part1);
    aoc::register_day_handler(19, 2, days::day19::part2);

    // Parse args
	let args: Vec<String> = env::args().collect();

    // Find part
    let mut part = 1;
    if args.len() > 2 {
        let parsed_part = args[2].parse::<u32>();
        if let Ok(parsed_part) = parsed_part {
            part = parsed_part;
        } else {
		    eprintln!("Could not parse part from second argument: {}", args[2]);
    		process::exit(1);
        }
    }

    // Find input to use
	let mut sample = &format!("{}-example", part);
	if args.len() > 1 && args[1] != "-" {
		sample = &args[1];
	}

    // Find day
	let local = Local::now();
	let mut day = local.day();
    if args.len() > 3 {
        let parsed_day = args[3].parse::<u32>();
        if let Ok(parsed_day) = parsed_day {
            day = parsed_day;
        } else {
		    eprintln!("Could not parse day from third argument: {}", args[2]);
    		process::exit(1);
        }
    }

	// Load content
	let mut buf = String::with_capacity(1024);
	let file = File::open(format!("./input/{:02}/{}.txt", day, sample));
	if let Ok(mut file) = file {
		file.read_to_string(&mut buf).unwrap();
	} else {
		eprintln!("Could not load file for day {:02} with name {}.txt", day, sample);
		process::exit(1);
	}
    
    // Run it.
    aoc::run(day, part, &buf);
}
